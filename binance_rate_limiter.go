package exchange

import (
	"context"
	"gitlab.com/beearn/entity"
	"go.uber.org/ratelimit"
)

const (
	ratePerSecond = 6
)

type BinanceRateLimit struct {
	rl      ratelimit.Limiter
	binance *Binance
}

func NewBinanceRateLimiter(binance *Binance) *BinanceRateLimit {
	return &BinanceRateLimit{
		rl:      ratelimit.New(ratePerSecond),
		binance: binance,
	}
}

func (b *BinanceRateLimit) GetSymbols(ctx context.Context, market int) ([]string, error) {
	b.rl.Take()

	return b.binance.GetSymbols(ctx, market)
}

func (b *BinanceRateLimit) Klines(ctx context.Context, symbol, period string, market int) ([]*entity.Kline, error) {
	b.rl.Take()

	return b.binance.Klines(ctx, symbol, period, market)
}
func (b *BinanceRateLimit) KlinesFrom(ctx context.Context, symbol string, period string, startTime int, limit int, market int) ([]*entity.Kline, error) {
	b.rl.Take()

	return b.binance.KlinesFrom(ctx, symbol, period, startTime, limit, market)
}

func (b *BinanceRateLimit) Ticker(ctx context.Context, market int) ([]*entity.Crypto, error) {
	b.rl.Take()

	return b.binance.Ticker(ctx, market)
}

func (b *BinanceRateLimit) GetMarketStat(ctx context.Context, market int) ([]*entity.PriceChangeStats, error) {
	b.rl.Take()

	return b.binance.GetMarketStat(ctx, market)
}

func (b *BinanceRateLimit) GetOrder(ctx context.Context, symbol string, market int) ([]*entity.OrderInfo, error) {
	b.rl.Take()

	return b.binance.GetOrder(ctx, symbol, market)
}
