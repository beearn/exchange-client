package exchange

import (
	"context"
	"crypto/tls"
	"fmt"
	"github.com/adshao/go-binance/v2"
	"github.com/adshao/go-binance/v2/futures"
	"gitlab.com/beearn/entity"
	"net"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"sync/atomic"
	"time"
)

const (
	Spot = iota
	Futures
)

var counter int32 // Атомарный счетчик для индекса IP-адреса

// https://api.binance.com/api/v3/exchangeInfo
type Binance struct {
	spot     *binance.Client
	futures  *futures.Client
	proxyUrl string
	useIPs   []string
}

func WithProxy(proxyUrl string) Opt {
	return func(b *Binance) {
		b.proxyUrl = proxyUrl
	}
}

func WithOutIP(outIP []string) Opt {
	return func(b *Binance) {
		b.useIPs = outIP
	}
}

type Opt func(*Binance)

func NewBinance(apiKey, secretKey string, opts ...Opt) *Binance {
	b := &Binance{}
	for _, opt := range opts {
		opt(b)
	}

	spot := binance.NewClient(apiKey, secretKey)
	fr := futures.NewClient(apiKey, secretKey)

	if b.proxyUrl != "" {
		spot.HTTPClient.Transport = &http.Transport{
			Proxy: func(req *http.Request) (*url.URL, error) {
				return url.Parse(b.proxyUrl)
			},
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		fr.HTTPClient.Transport = &http.Transport{
			Proxy: func(req *http.Request) (*url.URL, error) {
				return url.Parse(b.proxyUrl)
			},
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
	}

	if b.useIPs != nil {
		whiteIPs := b.useIPs

		// Создание HTTP транспорта с кастомными параметрами
		transport := &http.Transport{
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).DialContext,
		}

		transport.DialContext = func(ctx context.Context, network, addr string) (net.Conn, error) {
			// Получение текущего значения счетчика и инкремент
			index := atomic.AddInt32(&counter, 1) - 1

			// Выбор IP-адреса из списка
			ip := whiteIPs[index%int32(len(whiteIPs))]

			// Создание Dialer с выбранным LocalAddr
			dialer := &net.Dialer{
				Timeout:   1 * time.Minute,
				KeepAlive: 1 * time.Minute,
				LocalAddr: &net.TCPAddr{
					IP: net.ParseIP(ip),
				},
			}

			return dialer.DialContext(ctx, network, addr)
		}

		// Создание HTTP клиента
		fr.HTTPClient = &http.Client{
			Transport: transport,
		}

		spot.HTTPClient = &http.Client{
			Transport: transport,
		}
	}

	if os.Getenv("USE_IP") != "" && os.Getenv("PORT") != "" {
		transport := &http.Transport{
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
				LocalAddr: &net.TCPAddr{
					IP: net.ParseIP(fmt.Sprintf("%s:%s", os.Getenv("USE_IP"), os.Getenv("PORT"))), // Используем первый белый IP адрес для исходящего соединения
				},
			}).DialContext,
		}

		fr.HTTPClient.Transport = transport
		spot.HTTPClient.Transport = transport
	}

	b.spot = spot
	b.futures = fr

	return b
}

func (b *Binance) GetSymbols(ctx context.Context, market int) ([]string, error) {
	var res []string
	resRaw, err := b.Ticker(ctx, market)
	if err != nil {
		return nil, err
	}
	for i := range resRaw {
		res = append(res, resRaw[i].Symbol)
	}

	return res, nil
}

func (b *Binance) Klines(ctx context.Context, symbol, period string, market int) ([]*entity.Kline, error) {
	var res []*entity.Kline
	switch market {
	case Spot:
		resRaw, err := b.spot.NewKlinesService().Symbol(symbol).Interval(period).Do(ctx)
		if err != nil {
			return nil, err
		}
		for _, v := range resRaw {
			kls := entity.Kline(*v)
			res = append(res, &kls)
		}
	case Futures:
		resRaw, err := b.futures.NewKlinesService().Symbol(symbol).Interval(period).Do(ctx)
		if err != nil {
			return nil, err
		}
		for _, v := range resRaw {
			kls := entity.Kline(*v)
			res = append(res, &kls)
		}
	}

	return res, nil
}

func (b *Binance) KlinesFrom(ctx context.Context, symbol, period string, startTime, limit, market int) ([]*entity.Kline, error) {
	var res []*entity.Kline

	switch market {
	case Spot:
		resRaw, err := b.spot.NewKlinesService().Symbol(symbol).Interval(period).StartTime(int64(startTime)).Limit(limit).Do(ctx)
		if err != nil {
			return nil, err
		}
		for _, v := range resRaw {
			kls := entity.Kline(*v)
			res = append(res, &kls)
		}
	case Futures:
		resRaw, err := b.futures.NewKlinesService().Symbol(symbol).Interval(period).Do(ctx)
		if err != nil {
			return nil, err
		}
		for _, v := range resRaw {
			kls := entity.Kline(*v)
			res = append(res, &kls)
		}
	}

	return res, nil

}

func (b *Binance) GetMarketStat(ctx context.Context, market int) ([]*entity.PriceChangeStats, error) {
	var res []*entity.PriceChangeStats
	switch market {
	case Spot:
		ticker, err := b.spot.NewListPriceChangeStatsService().Do(ctx)
		if err != nil {
			return nil, err
		}
		for _, v := range ticker {
			res = append(res, &entity.PriceChangeStats{
				Symbol:             v.Symbol,
				PriceChange:        v.PriceChange,
				PriceChangePercent: v.PriceChangePercent,
				WeightedAvgPrice:   v.WeightedAvgPrice,
				PrevClosePrice:     v.PrevClosePrice,
				LastPrice:          v.LastPrice,
				OpenPrice:          v.OpenPrice,
				HighPrice:          v.HighPrice,
				LowPrice:           v.LowPrice,
				Volume:             v.Volume,
				QuoteVolume:        v.QuoteVolume,
				OpenTime:           v.OpenTime,
				CloseTime:          v.CloseTime,
				FirstID:            v.FristID,
				LastID:             v.LastID,
				Count:              v.Count,
			})
		}
		return res, nil
	case Futures:
		ticker, err := b.futures.NewListPriceChangeStatsService().Do(ctx)
		if err != nil {
			return nil, err
		}
		for _, v := range ticker {
			res = append(res, &entity.PriceChangeStats{
				Symbol:             v.Symbol,
				PriceChange:        v.PriceChange,
				PriceChangePercent: v.PriceChangePercent,
				WeightedAvgPrice:   v.WeightedAvgPrice,
				PrevClosePrice:     v.PrevClosePrice,
				LastPrice:          v.LastPrice,
				OpenPrice:          v.OpenPrice,
				HighPrice:          v.HighPrice,
				LowPrice:           v.LowPrice,
				Volume:             v.Volume,
				QuoteVolume:        v.QuoteVolume,
				OpenTime:           v.OpenTime,
				CloseTime:          v.CloseTime,
				FirstID:            v.FristID,
				LastID:             v.LastID,
				Count:              v.Count,
			})
		}
	}

	return res, nil
}

func (b *Binance) GetOrder(ctx context.Context, symbol string, market int) ([]*entity.OrderInfo, error) {
	var orders []*entity.OrderInfo
	switch market {
	case Spot:
		ordersRaw, err := b.spot.NewListOpenOrdersService().Symbol(symbol).Do(ctx)
		if err != nil {
			return nil, err
		}
		for _, order := range ordersRaw {
			orders = append(orders, &entity.OrderInfo{
				Symbol:           order.Symbol,
				OrderID:          order.OrderID,
				ClientOrderID:    order.ClientOrderID,
				Price:            order.Price,
				OrigQuantity:     order.OrigQuantity,
				ExecutedQuantity: order.ExecutedQuantity,
				Status:           string(order.Status),
				TimeInForce:      string(order.TimeInForce),
				Type:             string(order.Type),
				Side:             string(order.Side),
				StopPrice:        order.StopPrice,
				Time:             order.Time,
				UpdateTime:       order.UpdateTime,
				Market:           market,
			})
		}
	case Futures:
		ordersRaw, err := b.futures.NewListOpenOrdersService().Symbol(symbol).Do(ctx)
		if err != nil {
			return nil, err
		}
		if len(ordersRaw) == 0 {
			return nil, nil
		}

		for _, order := range ordersRaw {
			orders = append(orders, &entity.OrderInfo{
				Symbol:           order.Symbol,
				OrderID:          order.OrderID,
				ClientOrderID:    order.ClientOrderID,
				Price:            order.Price,
				ReduceOnly:       order.ReduceOnly,
				OrigQuantity:     order.OrigQuantity,
				ExecutedQuantity: order.ExecutedQuantity,
				CumQuantity:      order.CumQuantity,
				CumQuote:         order.CumQuote,
				Status:           string(order.Status),
				TimeInForce:      string(order.TimeInForce),
				Type:             string(order.Type),
				Side:             string(order.Side),
				StopPrice:        order.StopPrice,
				Time:             order.Time,
				UpdateTime:       order.UpdateTime,
				WorkingType:      string(order.WorkingType),
				ActivatePrice:    order.ActivatePrice,
				PriceRate:        order.PriceRate,
				AvgPrice:         order.AvgPrice,
				OrigType:         order.OrigType,
				PositionSide:     string(order.PositionSide),
				PriceProtect:     order.PriceProtect,
				ClosePosition:    order.ClosePosition,
				Market:           market,
			})
		}
	}

	return orders, nil
}

// Ticker define ticker info
func (b *Binance) Ticker(ctx context.Context, market int) ([]*entity.Crypto, error) {
	var res []*entity.Crypto
	switch market {
	case Spot:
		prices, err := b.spot.NewListBookTickersService().Do(ctx)
		if err != nil {
			return nil, err
		}
		res = convertSpotTicker(prices)
	case Futures:
		prices, err := b.futures.NewListBookTickersService().Do(ctx)
		if err != nil {
			return nil, err
		}
		res = convertFuturesTicker(prices)
	}

	return res, nil
}

func convertFuturesTicker(prices []*futures.BookTicker) []*entity.Crypto {
	var data []*entity.Crypto
	var priceBid, priceAsk float64
	var err error
	t := time.Now()
	for _, p := range prices {
		priceBid, err = strconv.ParseFloat(p.BidPrice, 64)
		if err != nil || priceBid == 0 {
			err = nil
			continue
		}
		priceAsk, err = strconv.ParseFloat(p.AskPrice, 64)
		if err != nil || priceAsk == 0 {
			err = nil
			continue
		}
		data = append(data, &entity.Crypto{
			Symbol:     p.Symbol,
			Price:      (priceBid + priceAsk) / 2,
			LastUpdate: t,
		})
	}

	return data
}

func convertSpotTicker(prices []*binance.BookTicker) []*entity.Crypto {
	var data []*entity.Crypto
	var priceBid, priceAsk float64
	var err error
	t := time.Now()
	for _, p := range prices {
		priceBid, err = strconv.ParseFloat(p.BidPrice, 64)
		if err != nil || priceBid == 0 {
			err = nil
			continue
		}
		priceAsk, err = strconv.ParseFloat(p.AskPrice, 64)
		if err != nil || priceAsk == 0 {
			err = nil
			continue
		}
		data = append(data, &entity.Crypto{
			Symbol:     p.Symbol,
			Price:      (priceBid + priceAsk) / 2,
			LastUpdate: t,
		})
	}

	return data
}
