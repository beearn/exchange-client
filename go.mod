module gitlab.com/beearn/exchange-client

go 1.19

require (
	github.com/adshao/go-binance/v2 v2.4.5
	gitlab.com/beearn/entity v1.0.0
	go.uber.org/ratelimit v0.3.0
	google.golang.org/grpc v1.58.1
	google.golang.org/protobuf v1.31.0
)

require (
	github.com/benbjohnson/clock v1.3.0 // indirect
	github.com/bitly/go-simplejson v0.5.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	golang.org/x/net v0.14.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
	golang.org/x/text v0.12.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20230822172742-b8732ec3820d // indirect
)
