package exchange

import "time"

const (
	// "1m", "5m", "15m", "30m", "1h", "2h", "4h", "6h", "12h", "1d", "1w", "1m"
	Period1m  = "1m"
	Period3m  = "3m"
	Period5m  = "5m"
	Period15m = "15m"
	Period30m = "30m"
	Period1h  = "1h"
	Period2h  = "2h"
	Period4h  = "4h"
	Period6h  = "6h"
	Period8h  = "8h"
	Period12h = "12h"
	Period1d  = "1d"
	Period3d  = "3d"
	Period1w  = "1w"
	Period1M  = "1M"
)

var periodToDuration = map[string]time.Duration{
	Period1m:  time.Minute,
	Period3m:  3 * time.Minute,
	Period5m:  5 * time.Minute,
	Period15m: 15 * time.Minute,
	Period30m: 30 * time.Minute,
	Period1h:  time.Hour,
	Period2h:  2 * time.Hour,
	Period4h:  4 * time.Hour,
	Period6h:  6 * time.Hour,
	Period8h:  8 * time.Hour,
	Period12h: 12 * time.Hour,
	Period1d:  24 * time.Hour,
	Period3d:  3 * 24 * time.Hour,
	Period1w:  7 * 24 * time.Hour,
	Period1M:  30 * 24 * time.Hour,
}

func PeriodToDuration(period string) time.Duration {
	return periodToDuration[period]
}
